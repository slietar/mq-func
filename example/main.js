let svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');

document.querySelector('main').appendChild(svg);


let input = Object.entries({
  500: [464, 15],
  600: [528, 16],
  1000: [660, 20],
  [window.screen.width + 100]: [660, 20]
})
  .sort((a, b) => a[0] - b[0])
  .map(([x, ys]) => [parseInt(x), ...ys]);

let data = input[0].map((_y, yIndex) =>
  input.map((datapoint) => datapoint[yIndex])
);

let delta = 0.1;

let limits = data.map((points, index) => {
  let min = Math.min(...points) * (1 - delta);
  let max = Math.max(...points) * (1 + delta);

  return { min, max, range: max - min };
});


let size = {
  width: 162,
  height: 100
};

let margin = {
  top: 5,
  right: 5,
  bottom: 8,
  left: 12
};

svg.setAttribute('viewBox', `0 0 ${size.width} ${size.height}`);

// Axes
svg.innerHTML += `<path d="M${margin.left} ${margin.top} L${margin.left} ${size.height - margin.bottom} L${size.width - margin.right} ${size.height - margin.bottom}" class="axes" />`;


let graphSize = {
  width: size.width - margin.left - margin.right,
  height: size.height - margin.top - margin.bottom
};


let linspace = (min, max, n) => new Array(n).fill(0).map((_, index) => Math.round(min + (max - min) / (n - 1) * index));


let bottomAxis = 0;
let leftAxis = 1;

let bottomTicks = linspace(limits[0].min, limits[0].max - 40, 9);
let leftTicks = linspace(limits[1].min, limits[1].max - 5, 6);

for (let tick of bottomTicks) {
  svg.innerHTML += `<text x="${margin.left + (tick - bottomTicks[0]) / (bottomTicks.slice(-1)[0] - bottomTicks[0]) * graphSize.width}" y="${size.height - margin.bottom + 4}" class="ticks">${tick}</text>`;
}

for (let tick of leftTicks) {
  svg.innerHTML += `<text x="${margin.left - 5}" y="${size.height - margin.bottom - (tick - leftTicks[0]) / (leftTicks.slice(-1)[0] - leftTicks[0]) * graphSize.height}" class="ticks">${tick}</text>`;
}



let transform = ({ x, y }, xLimits, yLimits) => {
  return {
    x: margin.left + (x - xLimits.min) / xLimits.range * graphSize.width,
    y: size.height - margin.bottom - (y - yLimits.min) / yLimits.range * graphSize.height
  };
};


let [xs, ...series] = data;
let [xsLimits, ...seriesLimits] = limits;

let paths = series.map((points, seriesIndex) =>
  points.reduce((path, point, pointIndex) => {
    let cmd = pointIndex === 0 ? 'M' : 'L';
    let { x, y } = transform({ x: xs[pointIndex], y: point }, xsLimits, seriesLimits[seriesIndex]);
    return path + `${cmd}${x} ${y}`;
  }, '')
);


svg.innerHTML += `<path d="${paths[0]}" class="series" />`;


let segmentLengths = series.map((points, seriesIndex) =>
  new Array(points.length - 1).fill(0).map((_, pointIndex) =>
    distance(
      transform({ x: xs[pointIndex], y: points[pointIndex] }, xsLimits, seriesLimits[seriesIndex]),
      transform({ x: xs[pointIndex + 1], y: points[pointIndex + 1] }, xsLimits, seriesLimits[seriesIndex])
    )
  )
);

let totalLengths = segmentLengths.map((lengths) => lengths.reduce((sum, value) => sum + value, 0));


let circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');

circle.setAttribute('r', 1);
circle.style.setProperty('offset-path', `path("${paths[0]}")`);

let updateCirclePosition = () => {
  let value = predict(xs, 0, window.innerWidth);
  circle.style.setProperty('offset-distance', value * 100 + '%');
};

updateCirclePosition();

svg.appendChild(circle);


window.addEventListener('resize', () => {
  updateCirclePosition();
});


function predict(seriesX, seriesIndex, x) {
  if (x < seriesX[0]) {
    return 0;
  }

  let cumSum = 0;

  for (let index = 0; index < seriesX.length - 1; index += 1) {
    let startX = seriesX[index];
    let endX = seriesX[index + 1];

    if (x >= startX && x < endX) {
      return (cumSum + (x - startX) / (endX - startX) * segmentLengths[seriesIndex][index]) / totalLengths[seriesIndex];
    }

    cumSum += segmentLengths[seriesIndex][index];
  }

  return cumSum / totalLengths[seriesIndex];
}

function distance(a, b) {
  return Math.sqrt((a.x - b.x) ** 2 + (a.y - b.y) ** 2);
}
